package org.nostratech.spark.payload;

import lombok.*;

import java.util.List;

/**
 * Created by rennytanuwijaya on 2/5/16.
 */
@lombok.Data
public class DataMaster {
//    private List<Data> deviceData;
//    private List<Data> signalData;
    private LongLat longLat;
}
